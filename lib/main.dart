import 'package:flutter/material.dart';

Widget titleSection = Container(
    padding: const EdgeInsets.all(15),
    child: Row(children: [
      Expanded(
        /*1*/
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          /*2*/
          Container(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(
              'นางสาวศิริยากร มากแก้ว เพศหญิง อายุ 23 ปี',
              style: TextStyle(
                fontSize: 20,
                foreground: Paint()
                  ..style = PaintingStyle.fill
                  ..color = Colors.blueGrey[700]!,
              ),
            ),
          ),
        ]),
      ),
    ]));

Widget recordpresonal = Container(
  padding: const EdgeInsets.all(32),
  child: Row(children: [
    Expanded(
      /*1*/
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          /*2*/
          Container(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(
              'ประวัติส่วนตัว',
              style: TextStyle(
                fontSize: 20,
                foreground: Paint()
                  ..style = PaintingStyle.fill
                  ..color = Colors.blue[700]!,
              ),
            ),
          ),
          Text(
            'ชื่อ นางสาวศิริยากร มากแก้ว ชื่อเล่น ปุ้ม กำลังศึกษาอยู่ชั้นปีที่ 4',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          Text(
            'ศึกษาอยู่ที่คณะวิทยาการสารสนเทศ สาขาวิชา วิทยาการคอมพิวเตอร์ มหาวิทยาลัยบูรพา',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ],
      ),
    ),
  ]),
);

Widget mySkill = Container(
  padding: const EdgeInsets.all(15),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      Column(
        children: [
          Image.asset(
            'images/eclipse.jpg',
            width: 70,
            height: 70,
            fit: BoxFit.cover,
          ),
          const Text(
            'eclipse',
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
      Column(
        children: [
          Image.asset(
            'images/java.jpg',
            width: 70,
            height: 70,
            fit: BoxFit.cover,
          ),
          const Text(
            'Java',
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
      Column(
        children: [
          Image.asset(
            'images/nb.jpg',
            width: 70,
            height: 70,
            fit: BoxFit.cover,
          ),
          const Text(
            'NetBeans',
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
      Column(
        children: [
          Image.asset(
            'images/Visual.jpg',
            width: 70,
            height: 70,
            fit: BoxFit.cover,
          ),
          const Text(
            'Visual Studio Code',
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    ],
  ),
);

Widget education = Container(
  padding: EdgeInsets.only(top:5,bottom:10),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Text(
              'ประวัติการศึกษา',
              style: TextStyle(
                fontSize: 20,
                foreground: Paint()
                  ..style = PaintingStyle.fill
                  ..color = Colors.blue[700]!,
              ),
            ),
          

            ],)
        ],
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('โรงเรียนพิมายวิทยา',
              style: TextStyle(fontWeight: FontWeight.bold)),
          Text('มหาวิทยาลัยบูรพา',
              style: TextStyle(fontWeight: FontWeight.bold))
        ],
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            'images/phimai.jfif',
            width: 50,
            height: 50,
            fit: BoxFit.contain,
          ),
          Text(' '),
          Image.asset(
            'images/butapha.jpg',
            width: 40,
            height: 40,
            fit: BoxFit.contain,
          ),
        ],
      )
    ],
  ),
);

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
        backgroundColor: Colors.cyan[50],
        appBar: AppBar(
          title: const Text('My Resume'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/pum.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            titleSection,
            mySkill,
            recordpresonal,
            education
          ],
        ),
      ),
    );
  }
}
